\documentclass[11pt,twoside]{article} % twoside for both-sided print. could be unnecessary.

%%%%%%%%%%%%%%%%% PACKAGES %%%%%%%%%%%%%%%%%

%% Layout %%

\usepackage[paper=a4paper,inner=35mm,
outer=25mm,top=25mm,bottom=25mm]{geometry} 	% standard for thesis in Germany
\usepackage[onehalfspacing]{setspace}		% change the vertical line space easily
											% (onehalfspacing should be fine)
\usepackage[numbers,square,sort&compress]{natbib}   % good citation style
\usepackage{sectsty}                        % style section headings (e.g. to make them sans-serif)
\usepackage{fancyhdr}                       % what the name implies: fancy headers!

%% Environments %%

\usepackage{comment}
\usepackage{array} 				% more options tabular-Env. (fixed cell dimensions)
\usepackage{graphicx}			% graphics inclusion (figure-env.)
\usepackage{wrapfig}			% for text and images parallel (rather finicky)
\usepackage[format=hang,
labelfont=bf,center,position=top,
font=small]{caption}			% for great captions (figures, tables etc.)
								% options can be modified; I like these options
\usepackage{subcaption}			% enables subfigures and subtables with subcaptions;
								% subcaption package is dependend on caption package
\usepackage{multicol}			% for multicolumns in tables
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}

%% Design, Fonts, Characters %%

\usepackage[T1]{fontenc}		% encoding output with support for German
\usepackage[utf8]{inputenc}		% LaTeX only offers ASCII support - but UTF-8 is awesome
\usepackage[table,dvipsnames]{xcolor}	% great colors (dvipsnames hat weitere vordefinierte)
\usepackage{hyperref}			% for hyperlinks (\hyperref, \url{})
\usepackage{pgfplots}			% has tikz included! used to create simple graphs
\usepackage[linewidth=1pt]{mdframed} % boxes around text

%% Maths and Physics %%

\usepackage{siunitx}			% formatted units (\si{} for unit
					% \SI{}{} for number and unit
\usepackage{amsmath}			% basic math commands and env.
\usepackage{amssymb}			% basic math
\usepackage{mathtools}			% improves formatting of amsmath-stuff
\usepackage{bbm}				% because one needs to have \bbm{1} available
\usepackage{bm}					% slightly more comprehensive bold fonts
								% than covered by amsmath (bold greek letters)
\usepackage{physics}			% self-evidently for physics stuff
\usepackage{braket} 			% for \braket-command (handy in QM)

%% Fancy Equation Hyperreference Stuff

\usepackage{letltxmacro}		% for defining macros
\LetLtxMacro{\originaleqref}{\eqref} 	% save command that is due to be modified
\renewcommand{\eqref}{Gl.~\originaleqref}	% redefine



%%%%%%%%%%%%%%%%% PRE-DOCUMENT COMMANDS %%%%%%%%%%%%%%%%%

%% Layout-related stuff %%

%\setlength\parindent{0pt}		% no paragraph indent (matter of taste)
\setlength{\parskip}{0pt}		% no paragraph skip
\raggedbottom					% prevents unimaginably ugly page stretching
\pagestyle{fancy}               % from fancyhdr
\fancyhf{}                      % clears the header and footer, otherwise the elements of the default "plain" page style will appear
\fancyhead[LE,RO]{\scshape\nouppercase{\leftmark}}  % print section title in header in smallcaps, left on even pages (LE) and right on odd pages (RO)
\fancyhead[LO,RE]{\textsc{DMA}} % print experiment title in header in smallcaps, right on even pages (LE) and left on odd pages (RO)
\fancyfoot[LE,RO]{Seite \thepage} % print page number in foot, left on even and right on odd pages

\hypersetup{colorlinks=true,allcolors=Blue}

%% Colours %%

\definecolor{salmon}{HTML}{fa8072}			% these are colors that I use in
\definecolor{yellowgreen}{HTML}{9acd32}		% my matplotlib-plots regularly
\definecolor{steelblue}{HTML}{4682b4}		% and maybe want to reference in TeX
\definecolor{tableblue}{rgb}{0.925,0.925,1} 

%% SI-related stuff %%

\sisetup{inter-unit-product=\cdot} 		% makes units separated by a multiplication dot
\sisetup{output-decimal-marker = {.}}	% makes decimal marker a dot (better in German: comma)
\DeclareSIUnit\au{a.u.}				% defines atomic units
\DeclareSIUnit\kcal{kcal}			% defines kilo calories
\DeclareSIUnit\fs{fs}				% defines femtosecond
\DeclareSIUnit\as{as}				% defines attosecond
\DeclareSIUnit\hart{Hartree}		% defines Hartree

%% Tikz and PGF-plots %%

\pgfplotsset{width=5in, height=3in,
	     compat=1.9}			% sets PGF plot dimensions and makes all plots 
	     						% compatible with version 1.9 and further
\usepgfplotslibrary{external}	% tiks and PGF plot are pathetically slow. these options
\tikzexternalize				% ensure that each plot is compiled but once
								% and then stored and accessed from externally

%% Environments %%

\renewcommand{\arraystretch}{1.2}				% makes tables and arrays less cramped
\renewcommand{\figurename}{\textbf{Abb.}}	% caption name for figures
\renewcommand{\tablename}{\textbf{Tab.}}		% caption name for tables
\captionsetup[subfigure]{skip=-10pt}			% better subfigure-caption position
\renewcommand\thesubfigure{\alph{subfigure})}	% make subfigure captions increase
												% alphabetically
\renewcommand{\refname}{Literaturverzeichnis}	% library name


%%%%%%%%%%%%%%%%% NEW DEFINITIONS %%%%%%%%%%%%%%%%%

% Don't get overwhelmed; these are just definitions
% that I've gotten used to. These are all just shortcuts
% for existing definitions or small elements

\renewcommand{\i}{\mathbbm{i}}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\pa}{\partial}
\newcommand{\hamil}{\hat{H}}
\newcommand{\efeld}{\mathcal{E}}
\newcommand{\bfeld}{\mathcal{B}}
\newcommand{\afeld}{\mathcal{A}}
\newcommand{\efeldb}{\bm{\mathcal{E}}}
\newcommand{\bfeldb}{\bm{\mathcal{B}}}
\newcommand{\afeldb}{\bm{\mathcal{A}}}
\newcommand{\bmnab}{\bm{\nabla}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\D}{\mathrm{D}}
\newcommand{\eps}{\varepsilon}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\K}{\mathbb{K}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\rar}{\rightarrow}
\newcommand{\ex}{\mathbf{e}_x}
\newcommand{\ey}{\mathbf{e}_y}
\newcommand{\ez}{\mathbf{e}_z}
\newcommand{\er}{\mathbf{e}_r}
\newcommand{\et}{\mathbf{e}_\theta}
\newcommand{\ep}{\mathbf{e}_\varphi}
\newcommand{\vphi}{\varphi}
\newcommand{\vthe}{\vartheta}
\newcommand{\vtheta}{\vartheta}
\newcommand{\br}{\mathbf{r}}
\newcommand{\bp}{\mathbf{p}}
\newcommand{\bk}{\mathbf{k}}
\newcommand{\rmin}{r_\text{min}}
\newcommand{\rmax}{r_\text{max}}
\newcommand{\ddt}{\frac{\mathrm{d}}{\mathrm{d}t}}
\newcommand{\ppt}{\frac{\partial }{\partial t}}
\newcommand{\ddx}{\frac{\mathrm{d}}{\mathrm{d}x}}
\newcommand{\A}{\mathbf{A}}
\newcommand{\B}{\mathbf{B}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\inti}{\int_{-\infty}^\infty}



%%%%%%%%%%%%%%%%% DOCUMENT %%%%%%%%%%%%%%%%%


\begin{document}

\allsectionsfont{\sffamily} % make section titles sans-serif

\begin{center}

{
\Huge HiWi zur Implementierung des DMA Algorithmus in \textsc{Spicy}
}

\end{center}

\newpage

\tableofcontents

\newpage

\section{Einführung und Motivation}

Ausgangspunkt ist, dass wir eine Wellenfunktion durch eine quantenchemische Rechnung erhalten haben. Das hießt präzise, dass wir für einen bestimmten Basissatz die Koeffizienten für die Basisfunktionen gegeben haben. Hat man diese Wellenfunktion, kann man grundsätzlich sich alle ``stationären'' Erwartungswerte betrachten, darunter auch die Ladungsverteilung des Moleküls. Letzteres stellt sich als nicht trivial heraus. Die Populationsanalyse nach \textit{Mulliken} ist ein altes Tool, um diese Ladungsverteilung zu erhalten, stellt sich jedoch als ungenau heraus (bspw. für Dipolmomente). Qualitativ formulierte Argumente (``hier ist das Molekül stärker negativ geladen als dort'') benötigen keine Berechnung von elektrischen Multipolmomenten -- quantitative Betrachtungen jedoch schon. 

\subsection{Mulliken Analysis}

Nach \textit{Anthony Stone} ist die \textit{Mulliken analysis} nicht vollständig -- \textit{distributed multipole analysis} (DMA) behebt diese Unvollständigkeit. Daher hier ein kurzer Abriss zur \textit{Mulliken analysis}: Jedem Atom $A$ wird eine Atomladung $q$ zugeteilt:
\begin{equation}
q_A = Z_A - \sum_i n_{i,A}
\end{equation}
Hierbei ist $Z_A$ die formale Ladung des Atoms und $n_{i,A}$ die einzelnen Kontributionen des $i$-ten Orbitals (\textit{atom population}). Diese Kontributionen setzen sich zusammen als
\begin{equation}
n_{i,A} = \eta_i \sum_{a \in A} \Theta_{i,a}.
\end{equation}
Hierbei ist $\eta_i$ die \textit{orbital occupation number} (sind zwei Elekronen drin, oder doch nur eins). $\Theta_{i,a}$ ist die Population der Basisfunktion $a$ im $i$-ten Orbital (wir gehen ab jetzt einfach mal davon aus, dass alle Basisfunktionen reell sind...). Die Population ist definiert als
\begin{equation}
\Theta_{i,a} = C_{a,i}^2 + \sum_{b\neq a} C_{a,i} C_{b,i} S_{a,b}.
\end{equation}
Hier geht viel ab, also gehen wir alles nacheinander durch. $S_{a,b}$ ist das Überlappintegral der Basisfunktionen $\chi_a$ und $\chi_b$:
\begin{equation}
S_{a,b} = \int \chi_a (\br) \chi_b (\br) \d\br 
\end{equation}
Es sind $C$ die jeweiligen Koeffizienten des $i$-ten Molekülorbitals; wir erhalten also die Population der $a$-ten Basisfunktion für das Atom $A$, indem wir den Koeffizienten dieser Basisfunktion quadrieren und dazu einen ``Interaktionsterm'' addieren, welcher bei hohem Überlapp zweier Basisfunktionen (also Ähnlichkeit zweier Basisfunktionen) die Koeffizienten der beiden Basisfunktionen multipliziert. Das Ganze läuft tatsächlich über die Dichtematrix, welche für Hartree-Fock-Molekülorbitale eine $n\times n$-Matrix (für $n$ Basisfunktionen, die nicht zwangsweise reell sein müssen) ist:
\begin{equation}
D_{a,b} = 2 \sum_i C_{a,i} C_{b, i}^\ast
\end{equation}
Nun ist also die Populationsmatrix gegeben durch
\begin{equation}
P_{a,b} = D_{a,b} \cdot S_{a,b}.
\end{equation}
Das passt also alles; sind nur zwei verschiedene Arten der Formulierung.\footnote{Die Darstellungen beziehen sich auf das \textit{Sobereva Manual (Kap. 3.9.3)} und auf \url{https://en.wikipedia.org/wiki/Mulliken_population_analysis}.}

\subsection{DMA Einführung}

DMA soll eine simple, effiziente Methode für die Berechnung molekularer Multipolmomente beliebiger Ordnung mit hoher Genauigkeit sein. Es benutzt die eleganten analytischen Eigenschaften von Gaussfunktionen und ist somit in der Wahl der Basisfunktionen begrenzt. Es kann sowohl für CI-Wellenfunktionen als auch für simple SCF-Wellenfunktionen benutzt werden, da es nur die Ein-Elektron-Dichtematrix und eine Spezifikation der Basis benötigt. Die Qualität der Methode ist von der Wahl der Basis limitiert. \par 
Es wird eine Beschreibung der Ladungsdichte gesucht, welche erlaubt das Elektrostatische Potential \textit{außerhalb} der Ladungsdichte selbst zu berechnen. Auch berechnet werden können soll die elektrostatische und Induktionsenergie von Molekülen untereinander. Daher wird die räumliche Ausdehnung der Moleküle nicht berücksichtigt; ergo eine sphärische Ladungsverteilung wird betrachtet wie eine Punktladung. Das heißt nicht, dass die elektrostatischen Effekte durch Interpenetration von molekularen Ladungsdichten ignoriert werden kann -- nur halt nicht durch dieses Modell beschrieben. \par 
Die konventionelle Multipolentwicklung konvergiert erst bei langen Reichweiten \textcolor{red}{(was heißt das?)}. Die DMA-Methode ist kompakter und konvergiert schnell bei allen energetisch erreichbaren Positionen \textcolor{red}{(energetisch erreichbar?)}. Die Ladungsverteilung wird dabei \textit{ab initio} von der Wellenfunktion gewonnen, indem an verschiedenen Samplingpunkten im Molekül die Multipole berechnet werden (oft werden als Punkte die Atomzentren und Bindungszentren benutzt). Auch bei einer kleinen Anzahl an Samplingpunkten kann die Ladungsverteilung graphisch und akkurat repräsentiert werden.

\section{Die DMA Methode}

\subsection{Zusammenfassung} Die Multipolmomente, welche von jedem Paar an \textit{pure spherical Gaussian basis functions} erzeugt werden, werden respektive ihres Überlappzentrums berechnet und im Anschluss zur räumlich nächsten \textit{multipole site} verschoben. Diese \textit{multipole sites} sind üblicherweise die Atome selbst und/oder die Bindungszentren -- für Implementierung in \textsc{Spicy} ist jedoch eine hohe Flexibilität darin erforderlich, welche Punkte im Molekül als \textit{multipole sites} dienen. 

\subsubsection{Höhe der Multipole}

Welche Multipole auftreten hängt davon ab, welche Orbitale benutzt werden. Das Multipolmoment $Q_{lm}$ des Produkts zweier $s$-Funktionen ist eine Punktladung $Q_{00}$. Das vom Produkt einer $s$- und einer $p$-Funktion hat Punktladungs- und Dipolkomponenten. Das Produkt zweier $p$-Funktionen hat Punktladungs-, Dipol- und Quadrupolmomente.

\paragraph{Fahrplan}
\begin{enumerate}
\item Implementiere DMA ohne Shifting für H$_2$
\begin{itemize}
\item Nur $s$-$s$-Überlapp, also $Q_{00}$
\end{itemize}
\item Implementiere DMA ohne Shifting für HF oder H$_2$O
\begin{itemize}
\item zusätzlich $s$-$p$-Überlapp, also $Q_{1m}$
\end{itemize}
\item Implementiere DMA ohne Shifting für einfaches organisches Molekül (oder H$_2$O$_2$?) mit $p$-$p$-Bindungen
\begin{itemize}
\item zusätzlich $p$-$p$-Überlapp, also $Q_{2m}$
\end{itemize}
\item Implementiere Shift zu beliebigen Zentren
\end{enumerate}


\subsection{Startpunkt}

Der Startpunkt ist die Berechnung der Ein-Elektronen-Dichtematrix über eine Basis von \textit{gaussian-type} Orbitalen; wie auch bei der \textit{Mulliken analysis}. Die Idee an sich limitiert nicht die Art der Basisfunktion, jedoch die spezielle Durchführung, welche auf angenehmen Eigenschaften der kartesischen Gaussfunktionen basieren. Die Elektronendichte lässt sich in einer Gaussbasis bei bekannter Dichtematrix angeben als
\begin{equation}
\rho (\br) = \sum_{ij} \rho_{ij} \phi_i (\br) \phi_j (\br) = \sum_{ij} P_{ij} \left(\sum_s c_{is}\eta_s\right) \left( \sum_t c_{jt} \eta_t\right)
\end{equation}
Hierbei ist also $\rho_{ij}$ ein Element der Dichtematrix und $\phi$ die \textit{Gaussian basis functions} mit den \textit{cartesian primitive Gaussians}
\begin{equation}
\eta_i (\br) = (x-x_A)^p (y-y_A)^q (z-z_A)^r \exp[-\alpha (\br -\mathbf{r_A})^2],
\end{equation}
welche am Punkt $\mathbf{r_A}$ zentriert ist. Es gilt $l = p+q+r$, mit $l$ der \textit{angular momentum quantum number}.

\begin{comment}
\subsubsection{Definition des Multipolmoments}

Das Multipolmoment $Q_{lm}(\mathbf{P})$ ist definiert $R$ als
\begin{equation}
Q_{lm} (\mathbf{P}) = - \inti \mathcal{S}_{lm} (\br - \mathbf{P}) \sum_{s,t} \rho_{st} \eta_s \eta_t \: \d^3 r
\end{equation}
\end{comment}

\section{Obara-Saika-Rekursionsschema für Überlappintegrale}

\subsection{Primitive Cartesian Gaussians}

\subsubsection{Definition, Normierung, Produkt}

Wir betrachten einen bei $\mathbf{A}$ zentrierten Gaussian mit
\begin{equation}
G_{ijk}(\br, a, \mathbf{A}) = x_A^i y_A^j z_A^k \exp[-ar_A^2].
\end{equation}
Dieser faktorisiert zu
\begin{equation}
G_{ijk} (\br, a, \mathbf{A}) = G_i(x,a,A_x)G_j (y,a,A_y) G_k(z,a,A_z)
\end{equation}
mit bspw.
\begin{equation}
G_i (x,a,A_x) = x_A ^i \exp[-ax_A^2].
\end{equation}

\begin{mdframed}

Wichtig! Diese Gaussians sind \textbf{nicht} normiert! Normierung sollte mit der \textsc{Molden}-Konvention erfolgen. Leider ist nicht ganz klar, wie die gut zu implementieren ist.

\end{mdframed}
Eine andere Art der Normierung (nicht \textsc{Molden}-Konvention) erreicht man über den Selbstüberlapp
\begin{equation}
\Braket{G_i|G_i} = \frac{2i - 1)!!}{(4a)^i} \sqrt{\frac{\pi}{2a}}
\end{equation}
mit
\begin{equation}
n!! = 
\begin{cases} 1 & n=0 \\ 
n(n-2)(n-4)\cdots 2 & \text{even }n>0 \\ 
n(n-2)(n-4)\cdots 1 & \text{odd }n>0 \\
[(n+2)(n+4)\cdots 1]^{-1} & \text{odd }n<0
\end{cases}
\end{equation}


\subsubsection{Gaussian Product Rule}

Das Produkt zweier Gaussians ist durch
\begin{equation}
\exp[-ax_A^2] \cdot \exp[-bx_B^2] = \exp[-\mu X_{AB}^2] \cdot \exp[-px_P^2]
\end{equation}
gegeben mit \textit{total exponent} $p = a+b$, \textit{reduced exponent} $\mu = ab / (a+b)$, \textit{relative coordinate / Gaussian separation} $X_{AB} = A_x - B_x$ und der Schwerpunktskoordinate 
\begin{equation}
P_x = \frac{aA_x + bB_x}{a+b} = (aA_x + bB_x) / p
\end{equation}
Der Vorfaktor ist konstant und wird definiert als
\begin{equation}
K_{ab}^x = \exp[-\mu X_{AB}^2]
\end{equation}

\subsection{Überlappintegral}

\subsubsection{Definition}

\label{subsubsec:overlapdef}

Wir betrachten ein Überlappintegral der Form
\begin{equation}
S_{ab} = \int G_a G_b \: \d x \d y \d z
\end{equation}
mit kartesischen Gaussians $G_a$, $G_b$. Die Gaussians können wir faktorisieren, als ergibt sich:
\begin{equation}
S_{ab} = S_{ij}S_{kl}S_{mn}
\end{equation}
mit bspw.
\begin{equation}
S_{ij} = \inti \Omega_{ij}^x \d x 
\end{equation}
wobei $\Omega_{ij}^x$ die Überlappverteilung (also das Produkt der zweier Gaussians) mit
\begin{equation}
\Omega_{ij}^x = K_{ab}^x x_A^i x_B^j \exp[-px_P^2].
\end{equation}
Wir wollen nun den Gaussian komplett durch seine Zentrierung vom Punkt $\mathbf{P}$ beschreiben und erhalten dann mit
\begin{align}
x_A &= x_P + X_{PA} = x_P - \frac{b}{p} X_{AB} \\
x_B &= x_P + X_{PB} = x_P - \frac{a}{p} X_{AB}
\end{align}
nach gewissem Rechenaufwand
\begin{align}
\Omega_{ij}^x = K_{ab}^x \sum_k^{i+1} C_k^{ij} x_P^k \exp[-p x_P^2],
\end{align}
wobei $C_k^{ij}$ die Entwicklungskoeffizienten sind.

\subsubsection{Obara-Saika-Rekursionsschema}

Es gilt nun
\begin{align}
S_{i,j+1} - S_{i+1,j} &= X_{AB} S_{ij} \ \ \ \ \ \textit{(horizontal recurrence)} \\
S_{i+1,j} &= X_{PA} S_{ij} + \frac{1}{2p} (iS_{i-1,j} + jS_{i,j-1}) \label{eqref:obsai1} \\
S_{i,j+1} &= X_{PB} S_{ij} + \frac{1}{2p} (iS_{i-1,j} + jS_{i,j-1}).
\end{align}
Mit diesen drei Regeln kann man jedes Überlappintegral nun berechnen, wenn wir von
\begin{equation}
S_{00} = \sqrt{\frac{\pi}{p}} \exp[-\mu X_{AB}^2]
\end{equation}
aus starten.

\paragraph{Implementierung}

Das Obara-Saika-Rekursionsschema ist nicht eindeutig; viele Wege führen zum Ziel. In \textsc{Spicy} habe ich folgende Implementierung gewählt (inspiriert von Sebastian):\par 
Sei $S_{i,j}$ das zu berechnende Überlappintegral. Wir drücken nun $S_{i,j}$ rekursiv als Linearkombination von $S_{i', 0}$ über die \textit{horizontal recurrence} aus:
\begin{equation}
S_{i,j} = X_{AB} S_{i,j-1} + S_{i+1,j-1} \label{eq:horrecimpl}
\end{equation}
Nun vereinfacht sich \eqref{eqref:obsai1} von
\begin{align}
S_{i,j} &= X_{PA} S_{i-1,j} + \frac{1}{2p} [ (i-1) S_{i-2,j} + j S_{i-1, j-1} ]
\intertext{zu}
S_{i,0} &= X_{PA} S_{i-1,0} + \frac{(i-1)}{2p} S_{i-2,0}
\end{align}
Damit kann nun für alle $S_{i,0}$ eine Rekursionsformel gefunden werden, sodass alle Überlappintegrale aus \eqref{eq:horrecimpl} auf $S_{00}$ heruntergebrochen werden können.\par 
Offensichtlich ist diese Formel für $S_{1,0}$ für den Computer potentiell missverständlich, da dann möglicherweise der rechte Summand nicht ordentlich ausgewertet werden kann (es ist $S_{-1,0}$ nicht definiert -- muss es auch mathematisch nicht, da der rechte Summand eh durch den Faktor $(i-1)$ null wird, aber nen Computer könnte daran mglw. Anstoß nehmen). Also definieren wir $S_{1,0}$ separat als
\begin{equation}
S_{1,0} = X_{PA} S_{0,0}
\end{equation}
Damit ist die Berechnung eines jeden Überlappintegrals eineindeutig.

\subsubsection{Transformation zu \textit{pure spherical gaussians}}

%Es wird im folgenden eine leicht modifizierte (jedoch offensichtlich äquivalente) Notation verwendet, da dieser Abschnitt entnommen ist aus \textit{Multi-electron integrals} (S. Reine, T. Helgaker, R. Lindh, \textit{WIREs Comput Mol Sci} \textbf{2012}, 2: 290–303, doi: \url{10.1002/wcms.78}).\par 
Üblicherweise hantieren wir mit sphärischen Basisfunktionen der Form
\begin{equation}
G_{lm} (\br, a, \A) = \mathcal{S}_{lm} (x_A, y_A, z_A) \exp[-ar_A^2]
\end{equation}
wobei $S_{lm}$ die \textit{real solid harmonics} sind. Da wir nur für kartesische Gaussians die Rekursionsschemata besitzen (weil nur kartesische Gaussians oder hermitische Gaussians in ihren Richtungen separabel sind), entwickeln wir die sphärischen Gaussians in die Basis der kartesischen Gaussians. %mit
%\begin{align}
%G_{lm}(\br, a, \A) = \sum_{|\mathbf{i} = l|} S_{lm}^{\mathbf{i}} G_i (\br, a, \A)
%\end{align}
%wobei wir den Multiindex $\mathbf{i} = (i_x, i_y, i_z)^T$ mit $\mathbf{i} = i_x + i_y + i_z$ einführen. Es ergibt sich dann
%\begin{equation}
%G_\mathbf{i} (\br, a, \A) = G_{i_x} (a, x_A) G_{i_y} (a,y_A) G_{i_z} (a, z_A) = G_\mathbf{i} (\br, a, \A) = \br_A^\mathbf{i} \exp[-ar_A^2]
%\end{equation}
%aufgrund der Separabilität mit
%\begin{equation}
%\br_A^\mathbf{i} = x^{i_x}_A y^{i_y}_A z^{i_z}_A
%\end{equation}
%Hässlich ist nun, dass die Gaussians noch zusätzlich in eine \textit{contracted basis} transformiert werden müssen. Beispielsweise sind nun die \textit{contracted solid-harmonic overlap integrals} $S_{ab}$ gegeben durch
%\begin{align}
%S_{ab} = \underbrace{\sum_{\mathbf{ij}} S_{l_a, m_a}^\mathbf{i} S_{l_b, m_b}^\mathbf{j}}_{\textit{expansion into cart. basis}} \ \ \ \underbrace{\sum_{kl} c_k c_l S_{\mathbf{ij}}^{kl}}_{\textit{contraction step}}
%\end{align}
%wobei die primitiven Überlappintegrale
%\begin{equation}
%S_{\mathbf{ij}}^{kl} = \int G_\mathbf{i} (\br, a_k, \A) G_\mathbf{j} (\br, b_l, \B) \d^3 r
%\end{equation}
%nach Obara-Saika berechnet werden können.

%\begin{mdframed}
%Ich bin mir nicht im geringsten darüber klar, was die $S_{l_a,m_a}^\mathbf{i}$ für Entwicklungskoeffizienten sind. Das muss ich mal aufm Blatt Papier mir überlegen...
%\end{mdframed}

%Nun, ich habe es mir nun überlegt. Es ist unfassbar hässlich, aber sollte leider der richtige Ansatz sein.

\paragraph{Herleitung} Ausgangspunkt der Herleitung ist das Überlappintegral zweier sphärischer Basisfunktionen, wie sie Spicy ausgeben würde.

\begin{align}
S_{ab} &= \int \chi_a (\br) \chi_b (\br) \d\br 
\intertext{\textit{Contraction step}:}
 &= \sum_i \sum_j c_i c_j \int G_{lm}(\br, a_i, \A) G_{l'm'} (\br, b_j, \B) \d\br 
\intertext{Real solid harmonics mit $\br_A = \br - \A$:}
 &= \sum_i \sum_j c_i c_j \int \mathcal{S}_{lm} (\br_A) \e^{-a_i r_A^2} \mathcal{S}_{l'm'}(\br_B) \e^{-b_j r_B^2} \d\br 
\intertext{Entwicklung in homogene kart. Polynome \textit{(Mol. El.-Str. Theory, Gl. (6.4.47))}}
 &= \sum_i \sum_j c_i c_j \int \d\br \e^{-a_i r_A^2} \e^{-b_j r_B^2} \\
 & \ \ \ \cdot  \left[ N_{lm}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} C_{tuv}^{lm} x_A^{2t+|m|-2(u+v)} y_A^{2(u+v)} z_A^{l-2t-|m|} \right] \nonumber \\
 & \ \ \ \cdot \left[ N_{l'm'}^S \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{t'u'v'}^{l'm'} x_B^{2t'+|m'|-2(u'+v')} y_B^{2(u'+v')} z_B^{l'-2t'-|m'|} \right]  \nonumber
\intertext{Summen rumschieben:}
 &= \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S \nonumber \\
 & \ \ \ \cdot \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \int x_A^{2t+|m|-2(u+v)} y_A^{2(u+v)} z_A^{l-2t-|m|} \e^{-a_i r_A^2} \nonumber \\
 & \ \ \ \cdot x_B^{2t'+|m'|-2(u'+v')} y_B^{2(u'+v')} z_B^{l'-2t'-|m'|} \e^{-b_j r_B^2} \d\br
\intertext{Def. $q \coloneqq 2t+|m|-2(u+v)$ \& $r = 2(u+v)$ \& $s = l-2t-|m|$ (analog $q'$, $r'$ \& $s'$):}
&= \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S \nonumber \\
& \ \ \ \cdot \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \int G_{qrs} (\br, a_i, \A) G_{q'r's'} (\br, b_j, \B) \d \br 
\intertext{Identifiziere separable kartesische Überlappintegrale:}
 &= \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \nonumber \\
 & \ \ \ \cdot \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v_{m'}}^{[|m'|/(2-v_{m'})] + v_{m'}} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \cdot S^{(ij)}_{qq'} \cdot S^{(ij)}_{rr'} \cdot S^{(ij)}_{ss'}
\intertext{wobei}
C_{tuv}^{lm} &\coloneqq (-1)^{t+v-v_m} (1/4)^t \cdot \begin{pmatrix}
l \\ t
\end{pmatrix} \cdot \begin{pmatrix}
l - t \\ |m| + t 
\end{pmatrix} \cdot \begin{pmatrix}
t \\ u
\end{pmatrix} \cdot \begin{pmatrix}
|m| \\ 2v
\end{pmatrix} \\
N_{lm}^S &\coloneqq  \frac{1}{2^{|m|} l!} \sqrt{\frac{2(l+|m|)!(l-|m|)!}{2^{\delta_{0m}}}} \\
v_m &\coloneqq \begin{cases} 0 \ \ &m\geq 0 \\ 1/2 \ \ &m < 0 \end{cases}
\end{align}




\section{Obara-Saika-Rekursionschema für Multipolmomente}

\subsection{Multipolintegrale}

Die Voraussetzungen sind analog zu Abschnitt~\ref{subsubsec:overlapdef}. Wir betrachten nun Integrale der Form
\begin{equation}
S_{ab}^{efg} = \Braket{G_a | x_C^e y_C^f z_C^g | G_b}
\end{equation}
und separieren diese zu
\begin{equation}
S_{ab}^{efg} = S_{ij}^e S_{kl}^f S_{mn}^g
\end{equation}
(es ist $G_{ikm} \equiv G_a$), um
\begin{equation}
S_{ij}^e = \Braket{G_i | x_C^e | G_j}
\end{equation}
und analog die anderen Komponenten zu erhalten. Dabei ist $\mathbf{C}$ das Zentrum der Multipolentwicklung.\par 
Wir werden im folgenden $\mathbf{C} = \mathbf{P}$ wählen. Dies sorgt an einer Stelle für eine Vereinfachung.

\begin{mdframed}

Es ist mir nicht absolut klar, ob diese Integrale gerade den Komponenten der kartesischen Multipoltensoren $Q$ entsprechen. Wir gehen im folgenden von einer Analogie zu Basisfunktionen aus.

\end{mdframed}

In Analogie zu Basisfunktionen interpretieren wir an folgendem Beispiel:
Wir suchen die kartesische Komponente $Q_{xzz}$ des Oktopoltensors. Die ist gerade gegeben durch
\begin{equation}
S_{ab}^{102} = \Braket{G_a | x_C^1 z_C^2 | G_b}.
\end{equation}
Dies bedeutet, dass die Ordnung des Tensors (Oktopol $=3$) gerader der Summe der Exponenten entspricht.

\subsection{Obara-Saika-Rekursionsschema}

Wir können analog zu Überlappintegralen nun ein Rekursionsschema aufstellen. Dieses werde ich nicht vollständig abtippen, da die Implementierung in \textsc{Spicy} nur einen Bruchteil des Rekursionsschemas erfordert.

\paragraph{Implementierung} Erneut ist das Rekursionsschema nicht eindeutig. Ich wähle folgenden Ansatz:
\begin{enumerate}
\item führe gegebenes $S_{ij}^e$ auf Linearkombination von $S_{i'j'}^0$ zurück
\begin{itemize}
\item Dies entspricht gerade den Überlappintegralen
\end{itemize}
\item Benutze Obara-Saika für Überlappintegrale, um die $S_{i'j'}^0$ auf Linearkombination von $S_{00}^0$ zurückzuführen
\end{enumerate}
Die dafür notwendige Formel ist durch (9.3.19) in \textit{Molecular Electronic-Structure Theory} gegeben:
\begin{align}
S_{ij}^e = X_{PC}S_{ij}^{e-1} + \frac{1}{2p} \left(i S_{i-1,j}^{e-1} + j S_{i,j-1}^{e-1} + (e-1) S_{ij}^{e-2} \right)
\intertext{Mit $X_{PC} = P_x - C_x = 0$ wegen $\mathbf{P} = \mathbf{C}$ folgt}
S_{ij}^e = \frac{1}{2p} \left(i S_{i-1,j}^{e-1} + j S_{i,j-1}^{e-1} + (e-1) S_{ij}^{e-2} \right)
\end{align}
Wie zu sehen ist nimmt die Ordnung der Multipole $e$ stetig ab. Leider nimmt auch die Ordnung $i$ und $j$ ab. Damit \textsc{Haskell} nicht \textit{stack overflow errors} auswirft, weil der Ausdruck $0 \cdot S_{ij}^{(-1)}$ so evaluiert wird, dass erst $S_{ij}^{(-1)}$ berechnet wird (was eine unendliche Rekursion ergibt, anstelle das \textsc{Haskell} einfach merkt, dass der Müll eh mit null multipliziert wird), muss jedoch ein umfangreiches \textit{pattern matching} für alle möglichen Fälle durchgeführt werden:
\begin{itemize}
\item $e == 0$ \textit{(call overlap integral)}
\item $i > 0$, $j > 0$, $e > 1$
\item $i > 0$, $j > 0$, $e == 1$
\item $i == 0$, $j > 0$, $e > 1$
\item $i == 0$, $j > 0$, $e == 1$
\item $i > 0$, $j == 0$, $e > 1$
\item $i > 0$, $j == 0$, $e == 1$
\item $i == 0$, $j == 0$, $e > 1$
\item $i == 0$, $j == 0$, $e == 1$
\end{itemize}

\subsection{Gewichtung mit Dichtematrix}

Es ist bekannt, dass 
\begin{equation}
Q_{lm}(\mathbf{a}) = \int \mathcal{S}_{lm} (\br - \mathbf{a}) \rho(\br) \d^3 r
\end{equation}
mit
\begin{equation}
\rho(\br) = \sum_{ts} P_{ts} \chi_s \chi_t.
\end{equation}
Offensichtlich fehlt eine Gewichtung mit der Dichtematrix in den bisherigen Überlegungen. Glücklicherweise wird dies im nächsten Abschnitt automatisch aufgegriffen werden müssen...


\subsection{Umrechnung kartesische zu sphärischen Multipolen}

\begin{comment}
\begin{mdframed}
Folgende Überlegungen sind nach längeren Überlegungen Bullshit.
\end{mdframed}

Die \textit{contracted solid-harmonic multipole integrals} $M_{ab}^{e,\C}$ mit
\begin{equation}
M_{ab}^{e,\C} = \int \chi_a (\br) \chi_b (\br) \br_\C ^e \d^3 r
\end{equation}
sind \textcolor{blue}{meines Verständnis nach} gegeben durch
\begin{align}
M_{ab}^{e,\C} = \sum_{\mathbf{ij}} S_{l_a, m_a}^\mathbf{i} S_{l_b, m_b}^\mathbf{j} \sum_{kl} c_k c_l \ M_{\mathbf{ij}}^{kl^{e,\C}}
\end{align}
wobei die primitiven Multipolmomente
\begin{equation}
S_{\mathbf{ij}}^{kl^{e,\C}} = \int G_\mathbf{i} (\br, a_k, \A) G_\mathbf{j} (\br, b_l, \B) \br_\C^e \d^3 r
\end{equation}
nach Obara-Saika berechnet werden können.
\end{comment}

Wir betrachten das Überlappintegral an der Stelle $\mathbf{P}$ mit $\br - \mathbf{P} = \br_P$
\begin{align}
Q_{kq}(\mathbf{P}) = \sum_a \sum_b P_{ab} \int \mathcal{S}_{kq} (\br_P) \chi_a(\mathbf{r}_A) \chi_b (\mathbf{r}_B) \d^3 r
\end{align}
Betrachte nun:
\begin{align}
& \ \int \mathcal{S}_{kq} (\br - \mathbf{P}) \chi_s(\mathbf{r}_A) \chi_t (\mathbf{r}_B) \d^3 r 
\intertext{\textit{Contraction step:}}
&= \sum_i \sum_j c_i c_j \int  \mathcal{S}_{kq} (\br_P)G_{lm}(\br, a_i, \A) G_{l'm'} (\br, b_j, \B) \d\br 
\intertext{Real solid harmonics mit $\br_A = \br - \A$:}
 &= \sum_i \sum_j c_i c_j \int \mathcal{S}_{kq} (\br_P) \mathcal{S}_{lm} (\br_A) \e^{-a_i r_A^2} \mathcal{S}_{l'm'}(\br_B) \e^{-b_j r_B^2} \d\br 
\intertext{Entwicklung in homogene kart. Polynome \textit{(Mol. El.-Str. Theory, Gl. (6.4.47))}}
 &= \sum_i \sum_j c_i c_j \int \d\br  \mathcal{S}_{kq} (\br_P) \e^{-a_i r_A^2} \e^{-b_j r_B^2} \\
 & \ \ \ \cdot  \left[ N_{lm}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} C_{tuv}^{lm} x_A^{2t+|m|-2(u+v)} y_A^{2(u+v)} z_A^{l-2t-|m|} \right] \nonumber \\
 & \ \ \ \cdot \left[ N_{l'm'}^S \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{t'u'v'}^{l'm'} x_B^{2t'+|m'|-2(u'+v')} y_B^{2(u'+v')} z_B^{l'-2t'-|m'|} \right]  \nonumber
\intertext{Summen rumschieben:}
 &= \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S \nonumber \\
 & \ \ \ \cdot \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \int \mathcal{S}_{kq} (\br_P) x_A^{2t+|m|-2(u+v)} y_A^{2(u+v)} z_A^{l-2t-|m|} \e^{-a_i r_A^2} \nonumber \\
 & \ \ \ \cdot x_B^{2t'+|m'|-2(u'+v')} y_B^{2(u'+v')} z_B^{l'-2t'-|m'|} \e^{-b_j r_B^2} \d\br
\intertext{Def. $q \coloneqq 2t+|m|-2(u+v)$ \& $r = 2(u+v)$ \& $s = l-2t-|m|$ (analog $q'$, $r'$ \& $s'$):}
&= \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S \nonumber \\
& \ \ \ \cdot \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \int \mathcal{S}_{kq} (\br_P) G_{qrs} (\br, a_i, \A) G_{q'r's'} (\br, b_j, \B) \d \br 
\intertext{Def. $q'' \coloneqq 2t''+|q|-2(u''+v'')$ \& $r'' = 2(u''+v'')$ \& $s'' = k-2t''-|q|$}
 &= \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S N_{kq}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \nonumber \\
& \ \ \ \cdot  \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \sum_{t''=0}^{(k-|q|)/2} \sum_{u''=0}^{t''} \sum_{v''=v''_m}^{[|q|/(2-v''_m)] + v''_m}  C_{t''u''v''}^{kq} \nonumber \\
 & \ \ \ \cdot \int x_P^{q''} y_P^{r''} z_P^{s''} G_{qrs} (\br, a_i, \A) G_{q'r's'} (\br, b_j, \B) \d \br \label{eq:multipoledisaster}
\end{align}
Nun folgt ein kleiner Throwback (\textit{copy paste}): Wir betrachten nun Integrale der Form
\begin{equation}
S_{ab}^{efg} = \Braket{G_a | x_P^e y_P^f z_P^g | G_b}
\end{equation}
und separieren diese zu
\begin{equation}
S_{ab}^{efg} = S_{ij}^e S_{kl}^f S_{mn}^g
\end{equation}
(es ist $G_{ikm} \equiv G_a$), um
\begin{equation}
S_{ij}^e = \Braket{G_i | x_C^e | G_j}
\end{equation}
und analog die anderen Komponenten zu erhalten. Also können wir \eqref{eq:multipoledisaster} ausdrücken als:
\begin{align}
& \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S N_{kq}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \nonumber \\
& \ \ \ \cdot  \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \sum_{t''=0}^{(k-|q|)/2} \sum_{u''=0}^{t''} \sum_{v''=v''_m}^{[|q|/(2-v''_m)] + v''_m}  C_{t''u''v''}^{kq} \cdot S_{qq'}^{q''^{(ij)}} S_{rr'}^{r''^{(ij)}} S_{ss'}^{s''^{(ij)}}
\end{align}
Und jetzt töte mich bitte. Insgesamt erhalten wir:
\begin{align}
Q_{kq}(\mathbf{P}) &= \sum_a \sum_b P_{ab} \int \mathcal{S}_{kq} (\br_P) \chi_a(\mathbf{r}_A) \chi_b (\mathbf{r}_B) \d^3 r \\
&= \sum_a \sum_b P_{ab} \sum_i \sum_j c_i c_j N_{lm}^S N_{l'm'}^S N_{kq}^S \sum_{t=0}^{(l-|m|)/2} \sum_{u=0}^t \sum_{v=v_m}^{[|m|/(2-v_m)] + v_m} \nonumber \\
& \ \ \ \cdot  \sum_{t'=0}^{(l'-|m'|)/2} \sum_{u'=0}^{t'} \sum_{v'=v'_m}^{[|m'|/(2-v'_m)] + v'_m} C_{tuv}^{lm} C_{t'u'v'}^{l'm'} \nonumber \\
 & \ \ \ \cdot \sum_{t''=0}^{(k-|q|)/2} \sum_{u''=0}^{t''} \sum_{v''=v''_m}^{[|q|/(2-v''_m)] + v''_m}  C_{t''u''v''}^{kq} \cdot S_{qq'}^{q''^{(ij)}} S_{rr'}^{r''^{(ij)}} S_{ss'}^{s''^{(ij)}}
\end{align}


\end{document}












