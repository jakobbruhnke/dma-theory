\contentsline {section}{\numberline {1}Einführung und Motivation}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Mulliken Analysis}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}DMA Einführung}{4}{subsection.1.2}%
\contentsline {section}{\numberline {2}Die DMA Methode}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Zusammenfassung}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Höhe der Multipole}{5}{subsubsection.2.1.1}%
\contentsline {paragraph}{Fahrplan}{5}{section*.2}%
\contentsline {subsection}{\numberline {2.2}Startpunkt}{5}{subsection.2.2}%
\contentsline {section}{\numberline {3}Obara-Saika-Rekursionsschema für Überlappintegrale}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Primitive Cartesian Gaussians}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Definition, Normierung, Produkt}{6}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Gaussian Product Rule}{6}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Überlappintegral}{7}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Definition}{7}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Obara-Saika-Rekursionsschema}{7}{subsubsection.3.2.2}%
\contentsline {paragraph}{Implementierung}{8}{section*.3}%
\contentsline {subsubsection}{\numberline {3.2.3}Transformation zu \textit {pure spherical gaussians}}{8}{subsubsection.3.2.3}%
\contentsline {paragraph}{Herleitung}{8}{section*.4}%
\contentsline {section}{\numberline {4}Obara-Saika-Rekursionschema für Multipolmomente}{10}{section.4}%
\contentsline {subsection}{\numberline {4.1}Multipolintegrale}{10}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Obara-Saika-Rekursionsschema}{11}{subsection.4.2}%
\contentsline {paragraph}{Implementierung}{11}{section*.5}%
\contentsline {subsection}{\numberline {4.3}Gewichtung mit Dichtematrix}{12}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Umrechnung kartesische zu sphärischen Multipolen}{12}{subsection.4.4}%
